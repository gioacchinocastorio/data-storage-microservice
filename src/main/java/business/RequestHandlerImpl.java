package business;

import business.messages.AuthParameters;
import business.pojo.Procedure;
import utils.customexceptions.DuplicateProcedureException;
import utils.storage.ProcedureStorageDAO;
import utils.token.TokenStrategy;

import java.io.IOException;
import java.util.List;

/**
 * Basic implementation fo the RequestHandler interface.
 * See the interface to get more details
 */
public class RequestHandlerImpl implements RequestHandler {

    /**
     * Ref to the preocedure store
     */
    private final ProcedureStorageDAO storageDAO;

    /**
     * Auth token handler
     */
    private final TokenStrategy tokenStrategy;

    /**
     * Constructor
     * @param storageDAO Ref to the preocedure store
     * @param tokenGen Auth token handler
     */
    public RequestHandlerImpl(ProcedureStorageDAO storageDAO,
                              TokenStrategy tokenGen) {
        this.storageDAO = storageDAO;
        tokenStrategy = tokenGen;
    }


    @Override
    public void createProcedure(Procedure procedure) {

        try {
            storageDAO.save(procedure);
        } catch (DuplicateProcedureException e) {
            // cannot happen
            System.err.println("ERROR: cannot create the procedure for DB fault");
            System.exit(1);
        }

    }

    @Override
    public void updateProcedure(Procedure procedure) throws IOException {

        // handled by the same procedure as the creation
        if (procedure.getId() == null) {
            throw new IOException();
        }
        this.createProcedure(procedure);
    }

    @Override
    public Procedure getProcedure(String pricedureID) {

        return storageDAO.getByID(pricedureID);

    }

    @Override
    public void deleteProcedure(String procedureID) {

        storageDAO.deleteByID(procedureID);

    }

    @Override
    public List<Procedure> getProcedures(String owner, int pageNumber, int pageRange) {

        return storageDAO.getProcedurePage(owner, pageNumber, pageRange);
    }

    @Override
    public boolean isRequestAuthorised(AuthParameters parameters) {
        return this.tokenStrategy.isTokenValid(parameters.getAuthToken(), parameters.getUserId());
    }
}
