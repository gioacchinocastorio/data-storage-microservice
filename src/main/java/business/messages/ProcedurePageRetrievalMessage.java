package business.messages;

import business.pojo.Procedure;
import com.google.gson.annotations.Expose;

/**
 * This class represents the content of the answer when the user asks for his procedure paged info
 */
public class ProcedurePageRetrievalMessage {

    /**
     * ID of the procedure in the system
     */
    @Expose
    private String id;

    /**
     * Label of the service associated to the procedure
     */
    @Expose
    private String serviceName;

    public ProcedurePageRetrievalMessage(Procedure procedure) {
        this.id = procedure.getId();
        this.serviceName = procedure.getServiceName();
    }

    public String getId() {
        return id;
    }

    public String getServiceName() {
        return serviceName;
    }
}
