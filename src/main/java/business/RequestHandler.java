package business;

import business.messages.AuthParameters;
import business.pojo.Procedure;
import utils.customexceptions.DuplicateProcedureException;

import java.io.IOException;
import java.util.List;

/**
 * This entity hides the business logic of the system.
 */
public interface RequestHandler {

    /**
     * Procedure creation
     *
     * @param procedure Procedure info specs
     * @throws DuplicateProcedureException Thrown if there is an equal procedure
     */
    void createProcedure(Procedure procedure) throws DuplicateProcedureException;

    /**
     * Procedure update
     *
     * @param procedure Procedure info specs
     * @throws DuplicateProcedureException Thrown if there is an equal procedure
     * @throws IOException                 Thrown if no id is specified
     */
    void updateProcedure(Procedure procedure) throws DuplicateProcedureException, IOException;

    /**
     * Procedure retrieveal
     *
     * @param pricedureID ID of the procedure in the system
     * @return requested procedure
     */
    Procedure getProcedure(String pricedureID);

    /**
     * Procedure deletion
     *
     * @param procedureID ID of the procedure in the system
     */
    void deleteProcedure(String procedureID);

    /**
     * Get list of procedure info (the retrieval system is paged)
     *
     * @param owner      ID in the system of the issuer (provided by the User Store)
     * @param pageNumber Number of the requested page
     * @param pageRange  Number of results in the page (if it exceeds the page size, it returns the maximum value)
     * @return procedure page info
     */
    List<Procedure> getProcedures(String owner, int pageNumber, int pageRange);
    /*
    pagesize: 10
    {pageNumber: 1, pageSize: 10, totalPages:12, items: [...]}*/

    /**
     * Check if the request is authorized according to this server
     *
     * @param parameters Authentication parameters object
     * @return true if the request is authorized
     */
    boolean isRequestAuthorised(AuthParameters parameters);


}
