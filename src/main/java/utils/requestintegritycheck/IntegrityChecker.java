package utils.requestintegritycheck;

/**
 * Class to check if the requests are well formed
 */
public interface IntegrityChecker {

    /**
     * Check if the new procedure / update request is well formed
     * @param jsonRequest  json serializing the procedure input request
     * @return true if well formed
     */
    boolean isProcedureWellFormed(String jsonRequest);

}
