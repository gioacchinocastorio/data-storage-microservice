package utils.storage;

import business.pojo.Procedure;
import utils.customexceptions.DuplicateProcedureException;

import java.util.List;

public interface ProcedureStorageDAO {

    Procedure getByID(String procedureID);

    void save(Procedure procedure) throws DuplicateProcedureException;

    List<Procedure> getProcedurePage(String userID, int page, int range);

    void deleteByID(String procedureID);

}
