package utils.storage;

import business.pojo.Procedure;

public interface ProcedureWrapper {

    /**
     * Return the wrapped procedure
     * @return unwrapped procedure messages
     */
    Procedure getWrappedProcedure();
}
