package utils.storage.morphia;

import business.pojo.Procedure;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import utils.storage.ProcedureWrapper;

@Entity(value = "procedures", noClassnameStored = true)
public class ProcedureMorphiaAdapter implements ProcedureWrapper {

    @Id
    private ObjectId procedureID;

    private String serviceName;
    private String procedure;

    private String owner;




    public ProcedureMorphiaAdapter() {
    }

    public ProcedureMorphiaAdapter(Procedure procedureToWrap) {

        // retrieve data from the user messages
        this.setProcedureID(procedureToWrap.getId());
        serviceName = procedureToWrap.getServiceName();
        procedure = procedureToWrap.getProcedure();
        owner = procedureToWrap.getOwner();


    }

    private String getProcedureID() {
        return procedureID.toString();
    }

    private void setProcedureID(String userID) {
        if (userID != null) {
            this.procedureID = new ObjectId(userID);
        }
    }

    @Override
    public Procedure getWrappedProcedure() {

        Procedure wrappedProcedure = new Procedure();

        wrappedProcedure.setId(this.getProcedureID());

        wrappedProcedure.setOwner(this.owner);
        wrappedProcedure.setProcedure(this.procedure);
        wrappedProcedure.setServiceName(this.serviceName);

        return wrappedProcedure;
    }

}
