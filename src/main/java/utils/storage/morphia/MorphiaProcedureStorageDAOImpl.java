package utils.storage.morphia;

import business.pojo.Procedure;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;
import utils.config.Configurator;
import utils.customexceptions.DuplicateProcedureException;
import utils.storage.ProcedureStorageDAO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MorphiaProcedureStorageDAOImpl implements ProcedureStorageDAO {

    private static final String CONFIGURATION_NAMESPACE = "MongoDB";

    private static final String SERVER_ADDRESS_CONFIG = "server_address";
    private static final String SERVER_PORT_CONFIG = "server_port";

    private static final String DB_USERNAME = "db_user_username";
    private static final String DB_PASSWORD = "db_user_password";
    private static final String DB_CREDENTIAL_STORE = "credential_store_name";

    private static final String SERVICE_STORE_NAME = "store_name";
    public static final String OWNER_ID = "owner";

    public static final String PAGE_SIZE_CONFIG = "page_size";

    /**
     * Proxy of the server
     */
    private Datastore datastore;

    /**
     * MongoDB server infos
     */
    private String mongodbAddress;
    private int mongoPort;

    // TODO ricorda che hai creato un nuovo utente
    /*
    use admin
    switched to db admin
    > db.createUser(
    ...   {
    ...     user: "myUserAdmin",
    ...     pwd: "abc123",
    ...     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
    ...   }
    ... )
     */

    /**
     * Name of the user stores
     */
    private String storeName;


    /**
     * User auth informations
     */
    private String storeUserName;
    private char[] userPassword;
    private String userStoreName;

    /**
     * Size of a page in the store
     */
    private int pageSize = 10;


    /**
     * DB client with configurations
     *
     * @param configurator Configuration wrapper
     */
    public MorphiaProcedureStorageDAOImpl(Configurator configurator) {

        this.retrieveConfigurations(configurator);

        this.setupClient();
    }

    /**
     * Setup of the morphia client
     */
    private void setupClient() {
        try {
            Morphia storageConfig = new Morphia();
            storageConfig.mapPackageFromClass(ProcedureMorphiaAdapter.class);


            MongoCredential credential = MongoCredential.createCredential(storeUserName, userStoreName, userPassword);
            MongoClient client = new MongoClient(new ServerAddress(mongodbAddress, mongoPort), Arrays.asList(credential));

            datastore = storageConfig.createDatastore(client, storeName);
            datastore.ensureIndexes();
        } catch (Exception e) {
            System.err.println("ERROR: it seems that the store is not available or you're not authorized to access its data");
            System.exit(1);
        }
    }

    /**
     * Retrieve infos from the configuration file
     *
     * @param configurator configuration wrapper
     */
    private void retrieveConfigurations(Configurator configurator) {

        // server address infos
        mongodbAddress = configurator.getConfiguration(CONFIGURATION_NAMESPACE, SERVER_ADDRESS_CONFIG);
        mongoPort = Integer.parseInt(configurator.getConfiguration(CONFIGURATION_NAMESPACE, SERVER_PORT_CONFIG));

        // access credential
        storeUserName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, DB_USERNAME);
        userPassword = configurator.getConfiguration(CONFIGURATION_NAMESPACE, DB_PASSWORD).toCharArray();
        userStoreName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, DB_CREDENTIAL_STORE);

        // store infos
        storeName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, SERVICE_STORE_NAME);

        // page size
        pageSize = Integer.parseInt(configurator.getConfiguration(CONFIGURATION_NAMESPACE, PAGE_SIZE_CONFIG));

    }

    @Override
    public Procedure getByID(String procedureID) {


        ProcedureMorphiaAdapter adapter;
        Procedure retrieved = null;

        adapter = datastore.get(ProcedureMorphiaAdapter.class, new ObjectId(procedureID));
        if (adapter != null) {
            retrieved = adapter.getWrappedProcedure();
        }
        return retrieved;
    }

    @Override
    public void save(Procedure procedure) throws DuplicateProcedureException {

        try {

            datastore.save(new ProcedureMorphiaAdapter(procedure));
        } catch (DuplicateKeyException e) {
            // if the user is already in the store
            throw new DuplicateProcedureException();
        }

    }

    @Override
    public List<Procedure> getProcedurePage(String userID, int page, int range) {

        List<Procedure> retrieved = new ArrayList<>();

        // calculation of the correct page
        page = computePage(page);

        // offset in the page
        range = computeRange(range);

        //  retrive limited range page
        Query<ProcedureMorphiaAdapter> queryResult = datastore.createQuery(ProcedureMorphiaAdapter.class)
                .field(OWNER_ID).equal(userID).offset(page).limit(range);

        // unwrap every downloaded procedure
        for (ProcedureMorphiaAdapter wrapper : queryResult) {
            retrieved.add(wrapper.getWrappedProcedure());
        }

        return retrieved;
    }

    private int computeRange(int range) {
        if (range > pageSize) {
            range = pageSize;
        } else if (range < 0) {
            range = 0;
        }
        return range;
    }

    private int computePage(int page) {
        page -= 1; // page start at 1
        if (page < 0) {
            page = 0;
        }
        page *= pageSize;
        return page;
    }

    @Override
    public void deleteByID(String procedureID) {

        Procedure procedureToDelete = getByID(procedureID);

        datastore.delete(new ProcedureMorphiaAdapter(procedureToDelete));

    }
}
