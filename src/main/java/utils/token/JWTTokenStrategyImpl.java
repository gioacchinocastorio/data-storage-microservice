package utils.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import utils.config.Configurator;

import java.io.UnsupportedEncodingException;

/**
 * Token JWT implementation.
 */
public class JWTTokenStrategyImpl implements TokenStrategy {

    /**
     * Config specs
     */
    private static final String CONFIGURATION_NAMESPACE = "JWT";
    private static final String SECURE_KEY = "key";
    private static final String ISSUER_NAME = "issuer";

    /**
     * Symmetric HMAC256 encryption key
     */
    private String encryptionKey;

    /**
     * Name of the token issue authority
     */
    private String issuerName;

    /**
     * Ref to the token gen algorithm
     */
    private Algorithm algorithm;

    @Override
    public boolean isTokenValid(String token, String userId) {

        boolean valid = true;

        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer(issuerName)
                .withSubject(userId)
                .build(); //Reusable verifier instance

        try {
            verifier.verify(token);

        } catch (JWTVerificationException e) {
            valid = false;
        }

        return valid;
    }

    /**
     * Constructor
     *
     * @param configurator configuration wrapper
     */
    public JWTTokenStrategyImpl(Configurator configurator) {

        this.configure(configurator);

        this.setupAlgorithm();



    }

    private void setupAlgorithm() {
        try {
            algorithm = Algorithm.HMAC256(encryptionKey);
        } catch (UnsupportedEncodingException e) {
            System.err.println("ERROR: Unable to retrieve the configurations!");
            System.exit(1);
        }
    }

    /**
     * Retrieve the configurations
     *
     * @param configurator configuration wrapper
     */
    private void configure(Configurator configurator) {

        encryptionKey = configurator.getConfiguration(CONFIGURATION_NAMESPACE, SECURE_KEY);
        issuerName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, ISSUER_NAME);

    }
}
