package utils.token;

/**
 * Token generation generic algorithm
 */
public interface TokenStrategy {

    /**
     * Check if the token is valid
     * @param token auth token
     * @param userid ID of the token owner
     * @return true if valid
     */
    boolean isTokenValid(String token, String userid);

}
