package Main;

import business.RequestHandler;
import business.RequestHandlerImpl;
import controllers.Controller;
import controllers.SparkRestControllerImpl;
import utils.config.Configurator;
import utils.config.ConfiguratorIniImpl;
import utils.customexceptions.DuplicateProcedureException;
import utils.requestintegritycheck.IntegrityChecker;
import utils.requestintegritycheck.JSONSchemaDraft6IntegrityCheckerImpl;
import utils.serialization.GsonSerializerImpl;
import utils.serialization.Serializer;
import utils.storage.ProcedureStorageDAO;
import utils.storage.morphia.MorphiaProcedureStorageDAOImpl;
import utils.token.JWTTokenStrategyImpl;
import utils.token.TokenStrategy;

import java.io.File;

/**
 * Application point of access
 */
public class App {

    public static void main(String[] args) throws DuplicateProcedureException {

        // configurations
        File configFile = new File("configs/config.ini");
        Configurator configurator = new ConfiguratorIniImpl(configFile);

        // utils
        TokenStrategy tokengen = new JWTTokenStrategyImpl(configurator);
        Serializer serializer = new GsonSerializerImpl();
        IntegrityChecker checker = new JSONSchemaDraft6IntegrityCheckerImpl(configurator);

        // store
        ProcedureStorageDAO storageDAO = new MorphiaProcedureStorageDAOImpl(configurator);

        // business logic
        RequestHandler requestHandler = new RequestHandlerImpl(storageDAO, tokengen);

        // start the web server
        Controller procedureStore = new SparkRestControllerImpl(serializer, configurator, requestHandler, checker);
        procedureStore.startListening();

    }
}
