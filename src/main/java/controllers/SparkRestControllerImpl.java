package controllers;

import business.pojo.Procedure;
import business.RequestHandler;
import business.messages.AuthParameters;
import business.messages.ErrorMessage;
import business.messages.ProcedurePageRetrievalMessage;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import spark.*;
import utils.config.Configurator;
import utils.customexceptions.DuplicateProcedureException;
import utils.requestintegritycheck.IntegrityChecker;
import utils.serialization.Serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * JavaSpark REST controller.
 */
public class SparkRestControllerImpl implements Controller {

    /**
     * Configuration information in the config.ini file
     */
    private static final String CONFIGURATION_NAMESPACE = "Spark";
    private static final String PORT_CONFIG = "port";

    /**
     * The web server will listen here
     */
    private int listeningPort = 0;

    /**
     * message format
     */
    private static final String COMMUNICATION_FORMAT = "application/json";

    /**
     * In a route, this is the parameter that contains the userID
     */
    private static final String USER_ID_PARAMETER = ":user";

    /**
     * In a route, this is the parameter that contains the procedure ID
     */
    private static final String PROCEDURE_ID_PARAMETER = ":procedure";

    /**
     * In a route, this is the parameter that contains the page ID
     */
    private static final String PAGE_NUMBER_PARAMETER = ":page";

    /**
     * In a route, this is the parameter that contains the offset of results in page ID
     */
    private static final String RANGE_IN_PAGE_PARAMETER = ":range";

    /**
     * Request header for authentication (contains the token)
     */
    private static final String TOKEN_HEADER = "Authorization";

    /**
     * Route components
     */
    private static final String ROUTE_WILDCARD = "*";
    private static final String SAME_PATH_ROUTE = "";

    /**
     * Default empty response
     */
    private static final String EMPTY_RESPONSE = "";


    /**
     * Model serializer (object to JSON)
     */
    private Serializer serializer;

    /**
     * Configuration wrapper
     */
    private Configurator configurations;

    /**
     * JSON integrity checker
     */
    private IntegrityChecker integrityChecker;


    /**
     * Business logic handler
     */
    private RequestHandler requestHandler;


    /**
     * Constructor
     *
     * @param serializer     Model serializer (object to JSON)
     * @param configurations Configuration wrapper
     * @param requestHandler Business logic handler
     * @param checker        JSON integrity checker
     */
    public SparkRestControllerImpl(Serializer serializer,
                                   Configurator configurations,
                                   RequestHandler requestHandler,
                                   IntegrityChecker checker) {
        this.serializer = serializer;
        this.configurations = configurations;
        this.requestHandler = requestHandler;
        this.integrityChecker = checker;

        this.getConfigurations();
    }

    @Override
    public void startListening() {
        this.configureListeningServer();
        this.setupRoutes();
        this.waitServerSetup();

    }

    /**
     * Start the server conf
     */
    private void configureListeningServer() {

        getConfigurations();

        // listening port
        try {
            Spark.port(listeningPort);
        } catch (Exception e) {
            System.err.println("ERROR: the selected port is occupied by another service");
            System.exit(1);
        }


    }

    /**
     * Get the configs from the wrapper
     */
    private void getConfigurations() {
        listeningPort = Integer.parseInt(configurations.getConfiguration(CONFIGURATION_NAMESPACE, PORT_CONFIG));
    }

    /**
     * Activate the routes on the server
     */
    private void setupRoutes() {

        // COORS allowance
        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new LogFilter(), new CheckJSONIntegrityFilter());

        Spark.path("/api", () -> {

                    Spark.path("/procedure/:user", () -> {

                        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new RequestPermissionFilter());

                        // procedure creation
                        Spark.post(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new ProcedureCreationUpdateRoute());

                        Spark.path("/:procedure", () -> {
                            // procedure update
                            Spark.put(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new ProcedureCreationUpdateRoute());
                            // procedure retrieval
                            Spark.get(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new ProcedureRetrievalRoute(), serializer::serialize);
                            // procedure deletion
                            Spark.delete(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new ProcedureDeletionRoute());
                        });


                    });

                    Spark.path("/procedures/:user/:page/:range", () -> {

                        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new RequestPermissionFilter());

                        // retrieval of the procedures
                        Spark.get(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new ProcedurePageRetrieval(), serializer::serialize);

                    });


                }
        );

        // system error handling
        Spark.internalServerError(new InternalErrorRoute()); // server fault
        Spark.notFound(new ResourceNotFoundRoute()); // resource not found

        // set all the responses as JSON
        Spark.after(((request, response) -> response.header("content-type", COMMUNICATION_FORMAT)));


    }


    /**
     * Wait for the server to set up
     */
    private void waitServerSetup() {

        // this blocks the thread
        Spark.awaitInitialization(); // Wait for server to be initialized

    }

    /**
     * Creation of response for badly structured requests
     *
     * @param response http response wrapper
     * @return error message
     */
    private Object setMalformedRequest(Response response) {
        Object responseContent;
        response.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
        responseContent = ErrorMessage.getMalformedRequest();
        return responseContent;
    }


    /**
     * Check if the user is authorized
     */
    private class RequestPermissionFilter implements Filter {

        @Override
        public void handle(Request request, Response response) throws HaltException {

            String userid = request.params(USER_ID_PARAMETER);
            String authToken = request.headers(TOKEN_HEADER);

            AuthParameters authParameters = new AuthParameters(authToken, userid);

            if (!requestHandler.isRequestAuthorised(authParameters)) {
                Spark.halt(HttpStatus.UNAUTHORIZED_401);
            }

        }
    }

    /**
     * Route for 500 error
     */
    private class InternalErrorRoute implements Route {

        @Override
        public Object handle(Request request, Response response) throws Exception {
            return serializer.serialize(ErrorMessage.getInternalError());
        }
    }

    /**
     * Route for 404 error
     */
    private class ResourceNotFoundRoute implements Route {

        @Override
        public Object handle(Request request, Response response) throws Exception {
            return serializer.serialize(ErrorMessage.getNotFoundError());
        }
    }

    /**
     * Checks if the request is is proper JSON
     */
    private class CheckJSONIntegrityFilter implements Filter {
        @Override
        public void handle(Request request, Response response) throws Exception {

            // if there is a data input request
            if (Objects.equals(request.requestMethod(), HttpMethod.POST.asString()) || Objects.equals(request.requestMethod(), HttpMethod.PUT.asString())) {
                String json = request.body();

                // check if the request is valid json
                try {
                    new JSONObject(json);
                } catch (JSONException e) {
                    Spark.halt(HttpStatus.BAD_REQUEST_400);
                }
            }
        }
    }

    /**
     * Route for procedure creation and update
     */
    private class ProcedureCreationUpdateRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            Object responseContent;
            String jsonProcedure = request.body();
            Procedure procedure;
            String owner = request.params(USER_ID_PARAMETER);
            String procedureID = request.params(PROCEDURE_ID_PARAMETER);

            if (integrityChecker.isProcedureWellFormed(jsonProcedure)) {

                // retrieve user from the json serialization
                procedure = (Procedure) serializer.deserialize(jsonProcedure, Procedure.class);

                procedure.setOwner(owner);

                // if the user request has an ID for the procedure
                if (procedureID != null) {

                    // check if the owner is also the user who made the request
                    if (!Objects.equals(owner, procedure.getOwner())) {
                        // if not throw unauthorized
                        Spark.halt(HttpStatus.UNAUTHORIZED_401);
                    }
                }

                try {

                    if (procedure.getId() == null) {
                        requestHandler.createProcedure(procedure);
                        response.status(HttpStatus.CREATED_201);
                        responseContent = EMPTY_RESPONSE;
                    } else {
                        requestHandler.updateProcedure(procedure);
                        response.status(HttpStatus.NO_CONTENT_204);
                        responseContent = EMPTY_RESPONSE;
                    }

                } catch (DuplicateProcedureException | IOException exception) { // duplicate index exception!!!

                    response.status(HttpStatus.BAD_REQUEST_400);
                    responseContent = setMalformedRequest(response);

                }


            } else {
                responseContent = serializer.serialize(setMalformedRequest(response));
            }


            return responseContent;
        }
    }

    /**
     * Bulk retrieval of the user's procedures (paged)
     */
    private class ProcedurePageRetrieval implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            Object responseContent;
            List<Procedure> retrieved = null;
            List<ProcedurePageRetrievalMessage> sentInfo = new ArrayList<>();
            String owner = "";
            int pageNumber = 0;
            int pageRange = 0;

            try {

                // retrieve the information from the route
                owner = request.params(USER_ID_PARAMETER);
                pageNumber = Integer.parseInt(request.params(PAGE_NUMBER_PARAMETER));
                pageRange = Integer.parseInt(request.params(RANGE_IN_PAGE_PARAMETER));

            } catch (NumberFormatException e) {
                // the user sent irregular infos for page and range
                System.err.println("ERROR: the user wants to retrieve a paged of procedures with incoherent limits");
                Spark.halt(HttpStatus.BAD_REQUEST_400);

            }

            // retrieve the info from the store
            retrieved = requestHandler.getProcedures(owner, pageNumber, pageRange);

            // generate only a list of IDs and service names
            for (Procedure procedure : retrieved) {

                // wrap answers and enqueue
                sentInfo.add(new ProcedurePageRetrievalMessage(procedure));

            }

            // 200 is default status for get
            responseContent = sentInfo;

            return responseContent;
        }
    }

    /**
     * Retrieve a specified procedure
     */
    private class ProcedureRetrievalRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            String procedureID = request.params(PROCEDURE_ID_PARAMETER);

            Procedure retrieved = requestHandler.getProcedure(procedureID);

            // if there is no content, show not found
            if (retrieved == null) {
                Spark.halt(HttpStatus.NOT_FOUND_404);
            }

            return retrieved;
        }
    }

    /**
     * Procedure deletion procedure
     */
    private class ProcedureDeletionRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            String procedureID = request.params(PROCEDURE_ID_PARAMETER);

            requestHandler.deleteProcedure(procedureID);

            response.status(HttpStatus.NO_CONTENT_204);

            return EMPTY_RESPONSE;
        }
    }

    /**
     * Log the incoming requests
     */
    private class LogFilter implements Filter {
        @Override
        public void handle(Request request, Response response) throws Exception {

            System.out.println("Received request");
            System.out.println("Method: " + request.requestMethod());
            System.out.println("Client address: " + request.ip());
            System.out.println("Route: " + request.pathInfo());
            System.out.println("Content: " + request.body());
            System.out.println("\n\n");


        }
    }

}
